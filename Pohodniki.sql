USE Naloga;
DROP TABLE IF EXISTS VČLANITEV, POHODNIK, OCENA, POHOD, DRUŠTVO;

CREATE TABLE DRUŠTVO (
  idDRUŠTVO INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  Naziv VARCHAR(45) NOT NULL,
  Lokacija MEDIUMTEXT NOT NULL,
  Uradne_ure DATETIME NOT NULL,
  Davčna_št INT NOT NULL,
  TRR VARCHAR(45) NOT NULL,
  Telefonska_št INT NOT NULL,
  E_pošta VARCHAR(45) NOT NULL
);

CREATE TABLE POHOD (
  idPOHOD INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  Naziv VARCHAR(45) NOT NULL,
  Datum DATE NOT NULL,
  Lokacija LONGTEXT NOT NULL,
  Zbirno_mesto VARCHAR(45) NOT NULL,
  Opis LONGTEXT NOT NULL,
  Težavnost INT NOT NULL,
  Trajanje DOUBLE NOT NULL,
  Obvezna_oprema MEDIUMTEXT NOT NULL,
  Razmere VARCHAR(45) NOT NULL,
  Organiziranost_prevoza VARCHAR(45) NOT NULL,
  Stroški_prevoza DOUBLE NOT NULL,
  Podatki_vodiča MEDIUMTEXT NOT NULL
);

CREATE TABLE OCENA (
  idOCENA INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  Ocena INT NOT NULL,
  Komentar LONGTEXT NOT NULL
);

CREATE TABLE POHODNIK (
  idPOHODNIK INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  Ime VARCHAR(45) NOT NULL,
  Priimek VARCHAR(45) NOT NULL,
  Naslov VARCHAR(45) NOT NULL,
  Telefonska_št INT NOT NULL,
  E_pošta VARCHAR(45) NOT NULL,
  EMŠO INT NOT NULL,
  Značka MEDIUMTEXT NOT NULL,
  Uporabniško_ime VARCHAR(45) NOT NULL,
  Geslo VARCHAR(45) NOT NULL,
  TK_DRUŠTVO INT NOT NULL,
  TK_POHOD INT NOT NULL,
  FOREIGN KEY (TK_DRUŠTVO) REFERENCES DRUŠTVO(idDRUŠTVO),
  FOREIGN KEY (TK_POHOD) REFERENCES POHOD(idPOHOD)
);

CREATE TABLE VČLANITEV (
  idVČLANITEV INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  Ime VARCHAR(45) NOT NULL,
  Priimek VARCHAR(45) NOT NULL,
  Naslov VARCHAR(45) NOT NULL,
  Davčna_št INT NOT NULL,
  Telefonska_št INT NOT NULL,
  TK_POHODNIK INT NOT NULL,
  FOREIGN KEY (TK_POHODNIK) REFERENCES POHODNIK(idPOHODNIK)
);

ALTER TABLE POHODNIK ADD CONSTRAINT TK_POHODNIK_DRUŠTVO
FOREIGN KEY (TK_DRUŠTVO) REFERENCES DRUŠTVO(idDRUŠTVO)  
ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE POHODNIK ADD CONSTRAINT TK_POHODNIK_POHOD
FOREIGN KEY (TK_POHOD) REFERENCES POHOD(idPOHOD)  
ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE VČLANITEV ADD CONSTRAINT TK_VČLANITEV_POHODNIK
FOREIGN KEY (TK_POHODNIK) REFERENCES POHODNIK(idPOHODNIK)  
ON DELETE RESTRICT ON UPDATE CASCADE;


INSERT INTO DRUŠTVO (Naziv, Lokacija, Uradne_ure, Davčna_št, TRR, Telefonska_št, E_pošta) VALUES
('Planinsko društvo Ljubljana', 'Ljubljana, Slovenija', '2024-05-16 09:00:00', 12345678, 'SI56123456789012345', 123456789, 'info@pd-ljubljana.si'),
('Planinsko društvo Maribor', 'Maribor, Slovenija', '2024-05-16 10:00:00', 23456789, 'SI66234567890123456', 234567890, 'info@pd-maribor.si'),
('Planinsko društvo Kranj', 'Kranj, Slovenija', '2024-05-16 11:00:00', 34567890, 'SI77345678901234567', 345678901, 'info@pd-kranj.si'),
('Planinsko društvo Celje', 'Celje, Slovenija', '2024-05-16 12:00:00', 45678901, 'SI88456789012345678', 456789012, 'info@pd-celje.si'),
('Planinsko društvo Koper', 'Koper, Slovenija', '2024-05-16 13:00:00', 56789012, 'SI99567890123456789', 567890123, 'info@pd-koper.si');

INSERT INTO POHOD (Naziv, Datum, Lokacija, Zbirno_mesto, Opis, Težavnost, Trajanje, Obvezna_oprema, Razmere, Organiziranost_prevoza, Stroški_prevoza, Podatki_vodiča) VALUES
('Pohod na Triglav', '2024-07-15', 'Triglav, Slovenija', 'Aljažev dom', 'Vzpon na najvišji vrh Slovenije.', 5, 8.5, 'Planinska oprema, Voda', 'Dobro', 'Ne', 0, 'Janez Novak, 041123456'),
('Pohod na Šmarno goro', '2024-06-10', 'Šmarna gora, Ljubljana', 'Pod goro', 'Kratek pohod na priljubljeno izletniško točko.', 2, 2.0, 'Lahka pohodniška oprema', 'Sončno', 'Ne', 0, 'Mojca Kranjc, 041234567'),
('Pohod na Vršič', '2024-08-20', 'Vršič, Slovenija', 'Erjavčeva koča', 'Vzpon na najvišji cestni prelaz v Sloveniji.', 3, 4.0, 'Planinska oprema', 'Spremenljivo', 'Da', 10, 'Andrej Kovač, 041345678'),
('Pohod na Stol', '2024-09-10', 'Stol, Slovenija', 'Valvasorjev dom', 'Vzpon na najvišji vrh Karavank.', 4, 6.0, 'Planinska oprema, Voda', 'Dobro', 'Ne', 0, 'Marija Horvat, 041456789'),
('Pohod na Krn', '2024-05-20', 'Krn, Slovenija', 'Planinski dom pri Krnskih jezerih', 'Vzpon na enega najbolj obiskanih vrhov v Posočju.', 3, 5.5, 'Planinska oprema, Voda', 'Dobro', 'Ne', 0, 'Boris Potočnik, 041567890');

INSERT INTO OCENA (Ocena, Komentar) VALUES
(5, 'Odličen pohod!'),
(4, 'Zelo lepo, a nekoliko zahtevno.'),
(3, 'Povprečno, pričakoval sem več.'),
(5, 'Izjemna izkušnja, priporočam vsem.'),
(2, 'Premalo organizirano.');

INSERT INTO POHODNIK (Ime, Priimek, Naslov, Telefonska_št, E_pošta, EMŠO, Značka, Uporabniško_ime, Geslo, TK_DRUŠTVO, TK_POHOD) VALUES
('Janez', 'Novak', 'Ljubljana, Slovenija', 41123456, 'janez.novak@gmail.com', 11111111, 'Turist', 'janez123', 'geslo123', 1, 1),
('Mojca', 'Kranjc', 'Maribor, Slovenija', 41234567, 'mojca.kranjc@gmail.com', 22222222, 'Pohodnik', 'mojca123', 'geslo789', 3, 3),
('Marija', 'Horvat', 'Celje, Slovenija', 41456789, 'marija.horvat@gmail.com', 44444444, 'Resni planinec', 'marija123', 'geslo321', 4, 4),
('Boris', 'Potočnik', 'Koper, Slovenija', 41567890, 'boris.potocnik@gmail.com', 55555555, 'Gorski gams', 'boris123', 'geslo654', 5, 5),
('Nina', 'Kovačič', 'Ljubljana, Slovenija', 41678901, 'nina.kovacic@gmail.com', 66666666, 'Resni planinec', 'nina123', 'geslo987', 1, 1),
('Matej', 'Zupan', 'Maribor, Slovenija', 41789012, 'matej.zupan@gmail.com', 77777777, 'Turist', 'matej123', 'geslo654', 2, 2),
('Petra', 'Mlakar', 'Kranj, Slovenija', 41890123, 'petra.mlakar@gmail.com', 88888888, 'Gorski gams', 'petra123', 'geslo321', 3, 3),
('Dejan', 'Majer', 'Celje, Slovenija', 41901234, 'dejan.majer@gmail.com', 99999999, 'Pohodnik', 'dejan123', 'geslo123', 4, 4),
('Sara', 'Matišič', 'Šentjur, Slovenija', 051789635, 'sarchi.mat8@gmail.com', 54786325, 'Turist', 'sarchi88', 'nekaj55', 2, 1),
('Tina', 'Kovač', 'Koper, Slovenija', 41012345, 'tina.kovac@gmail.com', 10101010, 'Resni planinec', 'tina123', 'geslo456', 5, 5);

INSERT INTO VČLANITEV (Ime, Priimek, Naslov, Davčna_št, Telefonska_št, TK_POHODNIK) VALUES
('Janez', 'Novak', 'Ljubljana, Slovenija', 12345678, 41123456, 1),
('Mojca', 'Kranjc', 'Maribor, Slovenija', 23456789, 41234567, 2),
('Marija', 'Horvat', 'Celje, Slovenija', 45678901, 41456789, 3),
('Boris', 'Potočnik', 'Koper, Slovenija', 56789012, 41567890, 4),
('Nina', 'Kovačič', 'Ljubljana, Slovenija', 67890123, 41678901, 5),
('Matej', 'Zupan', 'Maribor, Slovenija', 78901234, 41789012, 6),
('Petra', 'Mlakar', 'Kranj, Slovenija', 89012345, 41890123, 7),
('Dejan', 'Majer', 'Celje, Slovenija', 90123456, 41901234, 8),
('Sara', 'Matišič', 'Šentjur, Slovenija', 75136985, 051789635, 9),
('Tina', 'Kovač', 'Koper, Slovenija', 12345012, 41012345, 10);


